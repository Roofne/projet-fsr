package com.lip6.servlets;

import com.lip6.entities.Contact;
import com.lip6.entities.Address;
import com.lip6.entities.PhoneNumber;
import com.lip6.entities.ContactGroup;

import com.lip6.util.JpaUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnnuaireServlet extends HttpServlet {

    public AnnuaireServlet() { super(); }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");

        String street  = request.getParameter("street");
        String city    = request.getParameter("city");
        String zip     = request.getParameter("zip");
        String country = request.getParameter("country");

        String num     = request.getParameter("num");
        String kind    = request.getParameter("kind");

        String groupName = request.getParameter("groupName");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        Contact c = new Contact(fname, lname, email);
        Address a = new Address(street, city, zip, country);
        PhoneNumber p = new PhoneNumber(kind, num);
        ContactGroup g = new ContactGroup(groupName);

        // Ouverture de la transaction
        EntityManager em = JpaUtil.getEmf().createEntityManager(); // - [!] erreur
        // EntityManagerFactory emf = new Persistence.createEntityManagerFactory("projetFSR");
        // EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        // Relation entre les entities
        c.setAdd(a);
        c.addProfile(p);
        c.addBook(g);

        // Persitance
        em.persist(a);
        em.persist(p);
        em.persist(g);
        em.persist(c);

        // Validation de la transaction
        tx.commit();

        // Fermeture de la transaction
        em.close();

        // response handle
        response.sendRedirect("./");
    }
}
