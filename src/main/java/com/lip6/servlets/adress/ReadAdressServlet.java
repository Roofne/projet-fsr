package com.lip6.servlets.adress;

import com.lip6.daos.DAOAddress;
import com.lip6.entities.Address;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;

public class ReadAdressServlet extends HttpServlet {

    public ReadAdressServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String id = request.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOAddress dao = (DAOAddress) context.getBean("adressDAO");
        Address a = dao.readAddressByID(Integer.parseInt(id));

        // JSON Output
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.write(a.toJSON().toString());
    }
}