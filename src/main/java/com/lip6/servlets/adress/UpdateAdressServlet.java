package com.lip6.servlets.adress;

import com.lip6.daos.DAOAddress;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class UpdateAdressServlet extends HttpServlet {

    public UpdateAdressServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String id      = request.getParameter("id");
        String street  = request.getParameter("street");
        String city    = request.getParameter("city");
        String zip     = request.getParameter("zip");
        String country = request.getParameter("country");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOAddress dao = (DAOAddress) context.getBean("adressDAO");
        dao.updateAddress(Integer.parseInt(id),street,city,zip,country);
    }
}