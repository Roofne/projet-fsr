package com.lip6.servlets.contact;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

import com.google.gson.JsonObject;
import com.lip6.entities.Contact;
import com.lip6.util.PagePrinter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.lip6.daos.DAOContact;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ReadContactServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ReadContactServlet() { super(); }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String output = "";

        // parsing request parameters
		String id = request.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContact dao = (DAOContact) context.getBean("contactDAO");
        if(id == null) {
            List<Contact> contacts = dao.readContactAll();

            JsonObject temp = new JsonObject();
            for (int i=0; i < contacts.size(); i++) {
                temp.add(String.valueOf(i), contacts.get(i).toJSON());
            }
            output = temp.toString();
        } else {
            Contact contact = dao.readContactByID(Integer.parseInt(id));
            output = contact.toJSON().toString();
        }

        // JSON Output
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.write(output);
    }
}