package com.lip6.servlets.contact;

import com.lip6.daos.DAOContact;
import com.lip6.entities.Contact;
import com.lip6.util.PagePrinter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
public class UpdateContactServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String id = request.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContact dao = (DAOContact) context.getBean("contactDAO");
        Contact c = dao.readContactByID(Integer.parseInt(id));

        // HTML output
        PagePrinter out = new PagePrinter(response);
        out.printPageHeader();

        // Contact Form
        out.write("<form method='POST'>");
        out.write("<input type='text' name='fname' value='"+c.getFirstName()+"'>");
        out.write("<input type='text' name='lname' value='"+c.getLastName()+"'>");
        out.write("<input type='text' name='email' value='"+c.getEmail()+"'>");
        out.write("<input type='submit'>");
        out.write("<input type='reset' >");
        out.write("</form>");

        // HTML output
        out.printPageFooter();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String id    = request.getParameter("id");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContact dao = (DAOContact) context.getBean("contactDAO");
        dao.updateContact(Integer.parseInt(id), fname, lname, email);

        // response handle
        response.sendRedirect("../contact?id="+id);
    }
}
