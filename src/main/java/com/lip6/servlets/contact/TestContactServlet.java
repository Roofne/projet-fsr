package com.lip6.servlets.contact;

import com.lip6.component.ContactComponent;
import com.lip6.services.ContactService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestContactServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public TestContactServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        ContactComponent contactComponent = (ContactComponent) context.getBean(ContactComponent.class);
        contactComponent.readContactAll();

        ContactService contactService = (ContactService) context.getBean(ContactService.class);
        contactService.readContactAll();
    }
}
