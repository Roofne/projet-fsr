package com.lip6.servlets.contact;

import java.io.IOException;

import com.lip6.entities.Contact;
import com.lip6.util.PagePrinter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.lip6.daos.DAOContact;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CreateContactServlet extends HttpServlet {

    public CreateContactServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// HTML output
		PagePrinter out = new PagePrinter(response);
		out.printPageHeader();

		// Contact Form
		out.write("<form method='POST'>");
		out.write("<input type='text' name='fname'>");
		out.write("<input type='text' name='lname'>");
		out.write("<input type='text' name='email'>");
		out.write("<input type='submit'>");
		out.write("<input type='reset' >");
		out.write("</form>");

		// HTML output
		out.printPageFooter();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// parsing request parameters
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String email = request.getParameter("email");

		System.out.println(fname);

		// application context
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// DAO call
		DAOContact dao = (DAOContact) context.getBean("contactDAO");
		Contact c = dao.createContact(fname, lname, email);

		// response handle
		response.sendRedirect("../contact?id="+c.getId());
	}
}
