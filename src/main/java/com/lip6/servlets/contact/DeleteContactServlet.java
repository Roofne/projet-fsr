package com.lip6.servlets.contact;

import com.lip6.daos.DAOContact;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class DeleteContactServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // parsing request parameters
        String id = req.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContact dao = new DAOContact();
        dao.deleteContact(Integer.parseInt(id));
    }
}
