package com.lip6.servlets.group;

import com.google.gson.JsonObject;
import com.lip6.daos.DAOContactGroup;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;

public class CreateContactGroupServlet extends HttpServlet {

    public CreateContactGroupServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // parsing request parameters
        String groupName = request.getParameter("groupName");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContactGroup dao = (DAOContactGroup) context.getBean("groupDAO");
        dao.createContactGroup(groupName);
    }
}