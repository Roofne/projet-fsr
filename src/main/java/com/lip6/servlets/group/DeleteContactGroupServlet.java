package com.lip6.servlets.group;

import com.lip6.daos.DAOContactGroup;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class DeleteContactGroupServlet extends HttpServlet {

    public DeleteContactGroupServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("DELETE CONTACT GROUP");

        // parsing request parameters
        String id = request.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContactGroup dao = (DAOContactGroup) context.getBean("groupDAO");
        dao.deleteContactGroup(Integer.parseInt(id));
    }
}