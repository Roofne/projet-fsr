package com.lip6.servlets.group;

import com.google.gson.JsonObject;
import com.lip6.daos.DAOContactGroup;
import com.lip6.entities.ContactGroup;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ReadContactGroupServlet extends HttpServlet {

    public ReadContactGroupServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String output = "";

        // parsing request parameters
        String id = request.getParameter("id");

        // application context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // DAO call
        DAOContactGroup dao = (DAOContactGroup) context.getBean("groupDAO");
        if(id == null) {
            List<ContactGroup> groups = dao.readContactGroupAll();

            JsonObject temp = new JsonObject();
            for (int i=0; i < groups.size(); i++) {
                temp.add(String.valueOf(i), groups.get(i).toJSON());
            }
            output = temp.toString();
        } else {
            ContactGroup group = dao.readContactGroupByID(Integer.parseInt(id));
            output = group.toJSON().toString();
        }

        // JSON Output
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.write(output);
    }
}