package com.lip6.services;

import org.springframework.stereotype.Service;

@Service("contact")
public class ContactService {
    public void readContactAll() {
        System.out.println("CONTACT READ ALL (Service)");
    }
}
