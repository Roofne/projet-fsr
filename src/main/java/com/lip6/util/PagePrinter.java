package com.lip6.util;

import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class PagePrinter {

    PrintWriter out;

    public PagePrinter(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        this.out = response.getWriter();
    }

    public void printPageHeader() {
        this.out.write("<html>");
        this.out.write("<head>");
        this.out.write("<title>PROJET FSR</title>");
        this.out.write("<link rel='stylesheet' href='main.css'>");
        this.out.write("</head>");
        this.out.write("<body>");
    }

    public void printPageFooter() {
        this.out.write("</body>");
        this.out.write("</html>");

        this.printPageStyle();
    }

    public void printPageStyle() {
        this.out.write("<style>");
        this.out.write("body { background-color:lightgreen; }");
        this.out.write("main {" +
                "margin:0 auto;" +
                "width:90%" +
                "h1,h2 { font-color:white; }" +
                "}");
        this.out.write("form {" +
            "display:flex;" +
            "align-items: center;" +
            "border: 1px solid black;" +
            "border-radius: 5%;" +
            "background-color: white;" +
        "}");
        this.out.write("");
        this.out.write("");
        this.out.write("</style>");
    }

    public void write(String str) {
        this.out.write(str);
    }


}
