package com.lip6.util;

import java.io.InputStream;
import java.io.FileInputStream;

import java.util.Properties;

public class AppProperties extends Properties {

    public AppProperties() {
        super();

    	try {
			String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
			String appConfigPath = rootPath + "application.properties";
			InputStream in = new FileInputStream(appConfigPath);

            // instanciation
			this.load(in);
			
			// test
			// this.forEach((key, value) -> System.out.println("Key : " + key + ", Value : " + value));
		} catch (Exception e) {
			e.printStackTrace();
		}
    }    
}
