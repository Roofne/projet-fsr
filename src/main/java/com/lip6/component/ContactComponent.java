package com.lip6.component;

import org.springframework.stereotype.Component;

@Component
public class ContactComponent {
    public void readContactAll() {
        System.out.println("CONTACT READ ALL (Component)");
    }
}
