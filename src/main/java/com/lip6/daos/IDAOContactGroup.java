package com.lip6.daos;

import com.lip6.entities.ContactGroup;

import java.util.List;

public interface IDAOContactGroup {
    public ContactGroup createContactGroup(String groupName);
    public List<ContactGroup> readContactGroupAll();
    public ContactGroup readContactGroupByID(Integer id);
    public void updateContactGroup(Integer id, String groupName);
    public void deleteContactGroup(Integer id);
}
