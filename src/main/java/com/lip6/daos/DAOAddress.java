package com.lip6.daos;

import com.lip6.entities.Address;
import com.lip6.util.JpaUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class DAOAddress implements IDAOAddress {
    public Address createAddress(String street, String city, String zip, String country) {
        System.out.println("ADRESS CREATE");
        Address address = null;

        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            // Instanciation des objets metiers
            address = new Address(street, city, zip, country);
            em.persist(address);

            // Validation de la transaction
            em.getTransaction().commit();
            em.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }
    public List<Address> readAddressAll() {
        System.out.println("ADRESS READ ALL");
        List<Address> adress = null;

        try {
            EntityManager  em = JpaUtil.getEmf().createEntityManager();
            adress = em.createQuery("from Adress", Address.class).getResultList();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return adress;
    }

    public Address readAddressByID(Integer id) {
        System.out.println("ADRESS READ BY ID");
        Address address = null;

        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            address = em.find(Address.class, id);
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }
    public void updateAddress(Integer id, String street, String city, String zip, String country) {
        System.out.println("ADRESS UPDATE");

        // USING JPA
        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            // on met a jour le contact
            Address address = em.find(Address.class, id);
            address.setStreet(street);
            address.setCity(city);
            address.setZIP(zip);
            address.setCountry(country);
            em.merge(address);

            // Fermeture de la transaction
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteAddress(Integer id) {

        System.out.println("ADRESS DELETE");

        // USING JPA
        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            // on met a jour le contact
            Address a = em.find(Address.class, id);
            em.remove(a);

            // Fermeture de la transaction
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
