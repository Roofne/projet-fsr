package com.lip6.daos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.lip6.entities.Contact;
import com.lip6.util.JpaUtil;

public class DAOContact implements IDAOContact {

	// CREATE
	@Override
	public Contact createContact(String firstname, String lastname, String email) {

		System.out.println("CONTACT CREATE");
		Contact contact = null;

		// USING JPA
		try {
			// Ouverture de la transaction
			EntityManager     em = JpaUtil.getEmf().createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();

			// Instanciation des objets metiers
			contact = new Contact(firstname, lastname, email);

			// Sauvegarde de l'entite Contact
			em.persist(contact);

			// Validation de la transaction
			tx.commit();

			// Fermeture de la transaction
			em.close();

			System.out.println(contact);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return contact;
	}

	// READ

	public List<Contact> readContactAll() {
		System.out.println("CONTACT READ ALL");
		List<Contact> contacts = null;

		// USING JPA
		try {
			// Ouverture de la transaction
			EntityManager  em = JpaUtil.getEmf().createEntityManager();

			contacts = em.createQuery("from Contact", Contact.class).getResultList();

			// Fermeture de la transaction
			em.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return contacts;
	}
	public Contact readContactByID(Integer id) {
		;
		System.out.println("CONTACT READ BY ID");
		Contact contact = null;

		// USING JPA
		try {
			// Ouverture de la transaction
			EntityManager em = JpaUtil.getEmf().createEntityManager();

			// on recupère le contact
			contact = em.find(Contact.class, id);
			System.out.println(contact);

			// Fermeture de la transaction
			em.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return contact;
	}

	// UPDATE
	public void updateContact(Integer id, String firstname, String lastname, String email) {

		System.out.println("CONTACT UPDATE");

		// USING JPA
		try {
			// Ouverture de la transaction
			EntityManager em = JpaUtil.getEmf().createEntityManager();
			em.getTransaction().begin();

			// on met a jour le contact
			Contact contact = em.find(Contact.class, id);
			contact.setFirstName(firstname);
			contact.setFirstName(lastname);
			contact.setFirstName(email);
			em.merge(contact);

			// Fermeture de la transaction
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// DELETE
	public void deleteContact(Integer id) {

		System.out.println("CONTACT DELETE");

		// USING JPA
		try {
			// Ouverture de la transaction
			EntityManager em = JpaUtil.getEmf().createEntityManager();
			em.getTransaction().begin();

			// on met a jour le contact
			Contact contact = em.find(Contact.class, id);
			em.remove(contact);

			// Fermeture de la transaction
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TEST
	public void testContact(Integer id) {
		System.out.println("CONTACT TEST");
	}
}
