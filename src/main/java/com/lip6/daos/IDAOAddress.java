package com.lip6.daos;

import com.lip6.entities.Address;

import java.util.List;

public interface IDAOAddress {
    public Address createAddress(String street, String city, String zip, String country);
    public List<Address> readAddressAll();

    public Address readAddressByID(Integer id);
    public void updateAddress(Integer id, String street, String city, String zip, String country);
    public void deleteAddress(Integer id);
}
