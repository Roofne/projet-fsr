package com.lip6.daos;

import com.lip6.entities.PhoneNumber;

import java.util.List;

public interface IDAOPhoneNumber {
    public void createNumero(String street, String city, String zip, String country);
    public PhoneNumber readPhoneNumberByID(Integer id);
    public List<PhoneNumber> readPhoneNumberAll();
    public void updatePhoneNumber(Integer id, String street, String city, String zip, String country);
    public void deletePhoneNumber(Integer id);
}
