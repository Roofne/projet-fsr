package com.lip6.daos;

import com.lip6.entities.Contact;

import java.util.List;

public interface IDAOContact {

	public Contact createContact(String firstname, String lastname, String email);
	public List<Contact> readContactAll();
	public Contact readContactByID(Integer id);
	public void updateContact(Integer id, String firstname, String lastname, String email);
	public void deleteContact(Integer id);


}
