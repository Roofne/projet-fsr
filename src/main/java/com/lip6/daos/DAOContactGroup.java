package com.lip6.daos;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;
import com.lip6.util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class DAOContactGroup implements IDAOContactGroup {

    public DAOContactGroup() { super(); }

    // CREATE
    public ContactGroup createContactGroup(String groupName) {
        System.out.println("CONTACT CREATE");
        ContactGroup group = null;

        // USING JPA
        try {
            // Ouverture de la transaction
            EntityManager     em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            group = new ContactGroup(groupName);

            em.persist(group);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return group;
    }

    // READ
    public List<ContactGroup> readContactGroupAll() {
        System.out.println("CONTACT READ ALL");
        List<ContactGroup> groups = null;

        // USING JPA
        try {
            EntityManager  em = JpaUtil.getEmf().createEntityManager();
            groups = em.createQuery("from ContactGroup", ContactGroup.class).getResultList();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return groups;
    }
    public ContactGroup readContactGroupByID(Integer id) {
        System.out.println("CONTACT READ BY ID");
        ContactGroup group = null;

        // USING JPA
        try {
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            group = em.find(ContactGroup.class, id);
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return group;
    }

    // UPDATE
    public void updateContactGroup(Integer id, String groupName) {

        System.out.println("CONTACT UPDATE");

        // USING JPA
        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            // on met a jour le contact
            ContactGroup group = em.find(ContactGroup.class, id);
            group.setGroupName(groupName);
            em.merge(group);

            // Fermeture de la transaction
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // DELETE
    public void deleteContactGroup(Integer id) {
        System.out.println("CONTACT DELETE");

        // USING JPA
        try {
            // Ouverture de la transaction
            EntityManager em = JpaUtil.getEmf().createEntityManager();
            em.getTransaction().begin();

            // on met a jour le contact
            ContactGroup group = em.find(ContactGroup.class, id);
            em.remove(group);

            // Fermeture de la transaction
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
