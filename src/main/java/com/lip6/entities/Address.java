package com.lip6.entities;

import com.google.gson.JsonObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {
    // attributs
    private String street;
    private String city;
    private String zip;
    private String country;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id_address;

    // construteurs
    public Address() {}

    public Address(String _street, String _city, String _zip, String _country) {
        this.street  = _street;
        this.city    = _city;
        this.zip     = _zip;
        this.country = _country;
    }

    // getters
    public String  getStreet  () { return this.street;  }
    public String  getCity    () { return this.city;    }
    public String  getZIP     () { return this.zip;     }
    public String  getCountry () { return this.country; }

    // setters
    public void setStreet  (String  _street)  { this.street  = _street;  }
    public void setCity    (String  _city)    { this.city    = _city;    }
    public void setZIP     (String  _zip)     { this.zip     = _zip;     }
    public void setCountry (String  _country) { this.country = _country; }

    // adapter
    public JsonObject toJSON() {
        JsonObject out = new JsonObject();
        out.addProperty("street" ,this.street);
        out.addProperty("city"   ,this.city);
        out.addProperty("zip"    ,this.zip);
        out.addProperty("country",this.country);
        return out;
    }
}