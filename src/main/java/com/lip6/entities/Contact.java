package com.lip6.entities;

import com.google.gson.JsonObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Contact implements Serializable {
   
  	// Attributs
	private String  firstName;
	private String  lastName;
	private String  email;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	// Mapping
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="id_address")
	private Address address;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="contact")
	private Set<PhoneNumber> phones = new HashSet<>();

	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable(name="CTC_GRP",
			   joinColumns=@JoinColumn(name="id_contact"),
			   inverseJoinColumns=@JoinColumn(name="id_group")
	)
	private Set<ContactGroup> groups = new HashSet<>();;

    // Constructeurs
	public Contact() {}

	public Contact(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName  = lastName;
		this.email     = email;
	}

	public Contact(String firstName, String lastName, String email, Integer id) {
		this(firstName, lastName, email);
		this.id = id;
	}

	// Getters et setters
	public Integer getId()        { return id; }
	public String  getEmail()     { return email;}
	public String  getFirstName(){
		return this.firstName;
	}
	public String  getLastName()  { return this.lastName; }

	public void setId(Integer id)      { this.id = id; }

	public void setEmail(String email){
		this.email = email;
	}
	public void setFirstName(String firstname){
		this.firstName = firstname;
	}
	public void setLastName(String lastname)   { this.lastName = lastname; }

	// methodes
	@Override
	public String toString() {
		return this.firstName+" "+this.lastName+" : "+this.email;
	}

	public JsonObject toJSON() {
		JsonObject out = new JsonObject();
		out.addProperty("firstName" ,this.firstName);
		out.addProperty("lastName"  ,this.lastName);
		out.addProperty("email"     ,this.email);
		return out;
	}

	// relation
	public Address getAdd() { return this.address; }
	public Set<ContactGroup> getBooks() { return this.groups; }
	public Set<PhoneNumber> getProfiles() { return this.phones; }

	public void setAdd(Address add) { this.address = add; }
	public void setBooks(Set<ContactGroup> books) { this.groups = books; }
	public void setProfiles(Set<PhoneNumber> phones) {this.phones = phones; }
	public void addBook(ContactGroup book) { this.groups.add(book); }
	public void addProfile(PhoneNumber phone) { this.phones.add(phone); }

}
