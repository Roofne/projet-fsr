package com.lip6.entities;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class ContactGroup {

    // attributs
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer groupID;

    public String  groupName;

    @ManyToMany(mappedBy="groups")
    private Set<Contact> contacts = new HashSet<Contact>();

    // constructeurs
    public ContactGroup() {}

    public ContactGroup(String _groupName) {
        this.groupName = _groupName;
    }

    // getters et setters
    public Integer getGroupID   () { return this.groupID;   }
    public String  getGroupName () { return this.groupName; }

    public void addContact   (Contact _contact)  { this.contacts.add(_contact); }
    public void setGroupID   (Integer _id)       { this.groupID   = _id;        }
    public void setGroupName (String _groupName) { this.groupName = _groupName; }

    // methodes
    public JsonObject toJSON() {
        JsonObject out = new JsonObject();
        out.addProperty("groupName" ,this.groupName);
        return out;
    }
}