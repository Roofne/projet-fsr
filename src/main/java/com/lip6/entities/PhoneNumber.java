package com.lip6.entities;

import com.google.gson.JsonObject;
import jakarta.persistence.ManyToOne;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class PhoneNumber {

    // attributs
    private String  kind;
    private String  number;

    @ManyToOne
    @JoinColumn(name="id_contact")
    private Contact contact=null;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id_phone;

    // constructeurs
    public PhoneNumber() {}

    public PhoneNumber(String _kind, String _number) {
        this.kind   = _kind;
        this.number = _number;
    }

    // getters et setters
    public Contact getContact     () { return this.contact; }
    public String  getPhoneNumber () { return this.number;  }
    public String  getPhoneKind   () { return this.kind;    }

    public void setContact     (Contact _contact) { this.contact = _contact; }
    public void setPhoneNumber (String  _number)  { this.number  = _number;  }
    public void setPhoneKind   (String  _kind)    { this.kind    = _kind;    }

    // methodes
    public JsonObject toJSON() {
        JsonObject out = new JsonObject();
        out.addProperty("kind"   ,this.kind);
        out.addProperty("number" ,this.number);
        return out;
    }
}
